using System;
using Xunit;
using WebApplicationtest.Controllers;
using System.Linq;

namespace XUnitTestProjectxunit
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var con1 = new ValuesController();
            var result = con1.Get();

            Assert.Equal(2, result.Count());
        }
    }
}
